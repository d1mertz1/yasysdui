require 'open3'
require_relative 'command_button'
require_relative 'service_list'
require_relative 'system_service'

# controller class for a service
class ServiceControler
  attr_accessor :service
  attr_accessor :start_bt
  attr_accessor :stop_bt
  attr_accessor :status_bt
  attr_accessor :restart_bt
  attr_accessor :txtv
  attr_accessor :listv
  CMD = 'sudo systemctl'.freeze

  def initialize(txtv, listv)
    @txtv = txtv
    init_textv
    @listv = listv
    @start_bt = CommandButton.new('Start', self)
    @stop_bt = CommandButton.new('Stop', self)
    @status_bt = CommandButton.new('Status', self)
    @restart_bt = CommandButton.new('Restart', self)
    connect_list_sel_to_textv
  end

  def init_textv
    @txtv.text = 'No service selected'
  end

  def connect_list_sel_to_textv
    @listv.signal_connect_sel_changed(self)
  end

  def execute(action)
    return unless @service

    @action_output, @action_call_status =
      Open3.capture2e("#{CMD} #{action.downcase}  #{@service}")
    ui_update(action)
  end

  def execute_with(service, action)
    @service = service
    execute(action)
  end

  def ui_update(action)
    # (always) update textView buffer text
    @txtv.text = "#{action}: #{@service.descr} " \
      "\n Result: #{@action_call_status}" \
      "\n" + @action_output
    # update status info in the result list
    if action.casecmp('status').zero?
      @status_output = @action_output
    else
      # not sure if this makes senses in case systemctl call would
      # be non-blocking but otherwise it makes
      @status_output, _status_call_status =
        Open3.capture2e("#{CMD} status  #{@service}")
    end
    @service.read_status_info_from_output(@status_output)
    @listv.update_selected_row
  end
end
