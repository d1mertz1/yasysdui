require 'parseconfig'
require 'singleton'

# Allows optional configuration
class YSConfig
  include Singleton
  attr_reader :managed_services
  def load
    filename = File.join(Dir.home, '.config', 'yasysdui.conf')
    return unless File.exist?(filename)

    begin
      config = ParseConfig.new(filename)
    rescue e
      puts 'config file could not be read'
    end
    return unless config

    @managed_services = config.params['managed_services'].split(/\s+/) \
      if config.params['managed_services']
  end
end
