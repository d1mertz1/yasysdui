require 'singleton'
require_relative 'service_catalog'
require 'pry'

# colun ID's contstant for the gtk list
module ColumnID
  UNIT = 0
  LOAD = 1
  ACTIVE = 2
  SUB = 3
  DESCR = 4
  NATIVE_INFO = 5
  STATE = 6
end

# A TreeModelFilter to handle the fastsearch filtering
class FilteredServiceListStore < Gtk::TreeModelFilter
  include Singleton
  attr_accessor :filterstr
  attr_accessor :service_catalog
  def initialize
    model = ServiceListStore.new_all_service
    model.sort_by_unit
    super(model)
    @service_catalog = model.service_catalog
  end

  # quick and dirty but working fast search (uses a full text index)
  def set_visible_func_fastsearch
    set_visible_func { |_model, iter| fastsearch_visible_func(iter) }
  end

  def fastsearch_visible_func(iter)
    return true if @filterstr.nil?
    return true if @filterstr.length < 2

    rset = @service_catalog.idx[@filterstr]
    return rset.include?(iter.get_value(ColumnID::UNIT)) if rset
  end
end

# gtkliststore
class ServiceListStore < Gtk::ListStore
  include ColumnID
  attr_accessor :service_catalog
  def self.new_all_service
    sl = ServiceListStore.new
    sl.load_catalog
    sl
  end

  def initialize
    super(String, String, String, String, String, String, String)
  end

  def load_catalog
    @service_catalog = ServiceCatalog.instance
    puts "Loading Catalog with #{@service_catalog.size} item(s)"
    @service_catalog.each_value { |s| append_service(s) }
  end

  def reload
    clear # clear Gtk::ListStore
    ServiceCatalog.instance.rebuild
    load_catalog
  end

  def append_managed_service(name)
    service = @service_catalog[name]
    append_service(service) if service
  end

  def append_service(service)
    row = append
    row[UNIT] = service.to_str
    row[LOAD] = service.load
    row[ACTIVE] = service.active
    row[SUB] = service.sub
    row[DESCR] = service.descr
    row[NATIVE_INFO] = service.native_info
    row[STATE] = service.state
  end

  def sort_by_unit
    set_sort_column_id(UNIT, :ascending)
  end
end

# for colums with atoggle
class TreeViewColumnToggle < Gtk::TreeViewColumn
  CELL_RENDERER = Gtk::CellRendererToggle.new
  def initialize(title, column_id)
    attrs = { active: column_id }
    super(title, CELL_RENDERER, attrs)
    set_resizable(true)
    set_sort_column_id(column_id)
  end
end

# for text colums
class TreeViewColumnText < Gtk::TreeViewColumn
  CELL_RENDERER = Gtk::CellRendererText.new
  def initialize(title, column_id)
    attrs = { text: column_id }
    super(title, CELL_RENDERER, attrs)
    set_resizable(true)
    set_sort_column_id(column_id)
  end
end

# the list view
class ServiceListView < Gtk::TreeView
  include ColumnID
  def initialize(list)
    super(list)
    @list = list
    create_columns
    @col1.set_fixed_width(200)
    @col1.set_sort_indicator(true)
    append_columns
    set_enable_search(true)
  end

  def create_columns
    @col1 = TreeViewColumnText.new('Unit', UNIT)
    @col2 = TreeViewColumnText.new('Load', LOAD)
    @col3 = TreeViewColumnText.new('Active', ACTIVE)
    @col4 = TreeViewColumnText.new('Sub', SUB)
    @col5 = TreeViewColumnText.new('Description', DESCR)
    @col6 = TreeViewColumnText.new('Native?', NATIVE_INFO)
    @col7 = TreeViewColumnText.new('State', STATE)
  end

  def append_columns
    append_column(@col1)
    append_column(@col6)
    append_column(@col7)
    append_column(@col2)
    append_column(@col3)
    append_column(@col4)
    append_column(@col5)
  end

  def signal_connect_sel_changed(sctrl)
    @sel = selection
    @sel.set_mode(:browse)
    @sel.signal_connect('changed') do
      if @sel.selected
        @service = ServiceCatalog.instance[@sel.selected[UNIT]]
        sctrl.execute_with(@service, 'Status')
      else
        sctrl.init_textv
      end
    end
  end

  def update_selected_row
    iter = model.convert_iter_to_child_iter(@sel.selected)
    iter.set_value(LOAD, @service.load)
    iter.set_value(ACTIVE, @service.active)
    iter.set_value(SUB, @service.sub)
  end
end
