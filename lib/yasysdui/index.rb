require 'set'

# quick and dirty "Full text" indexer for fastsearch.
# works fine for small amounts of data
module Indexable
  def each_word
    each_sentence do |sen|
      sen.split(/[[:space:]]+|[[:punct:]]+/).each { |w| yield w }
    end
  end

  def each_wordstart
    each_word do |w|
      wstart = ''
      w.downcase.each_char do |c|
        wstart << c
        yield wstart.dup
      end
    end
  end
end

# poor man's full text index
class SimpleIndex < Hash
  def compile(obj)
    obj.each_wordstart do |ws|
      self[ws] = Set.new unless key?(ws)
      self[ws].add(obj)
    end
  end
end

# to be mixed-in in a collection of indexable objects
# the collection should be of kind Hash (uses each_value)
module Index
  attr_accessor :idx

  def build_index
    print 'Building fast search index... '
    @idx = SimpleIndex.new
    each_value do |srv|
      @idx.compile(srv)
    end
    puts 'done'
  end
end
