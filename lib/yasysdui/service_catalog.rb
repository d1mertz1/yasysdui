require 'singleton'
require_relative 'system_service'
require_relative 'config'
require_relative 'index'

module ServiceCatalogBase
  CMD_KNOWN_UNITS = 'sudo systemctl list-units -a -l ' \
    '-t service --no-pager --no-legend'.freeze
  INIT = '/etc/init.d'.freeze
  INIT_IGNORE = /README|.+~/
  ## UNIT       LOAD   ACTIVE SUB       DESCRIPTION
  RGXKU = Regexp.new('\A([[:graph:]]+)\.service\s+' \
    '([[:graph:]]+)\s+' \
    '([[:alnum:]]+)\s+' \
    '([[:alnum:]]+)\s+' \
    '([[:print:]]+)\Z')
  CMD_LIST_UNIT = 'sudo systemctl list-unit-files ' \
    '-a -l -t service --no-pager --no-legend'.freeze
  ## UNIT       STATE
  RGXLI = Regexp.new('\A([[:graph:]]+)\.service\s+' \
    '([[:graph:]]+)\Z')
end

# Collection of services
class ServiceCatalog < Hash
  include Singleton
  include ServiceCatalogBase
  include Index
  def initialize
    super
    @config = YSConfig.instance
    @config.load
    read_requested_services
    build_index
  end

  def rebuild
    clear
    read_requested_services
    build_index
  end

  def parse_know_unit(match)
    if match
      srv = SystemService.new(match[1])
      srv.load = match[2]
      srv.active = match[3]
      srv.sub = match[4]
      srv.descr = match[5]
      self[srv] = srv
    else
      puts l
      raise 'Failed parsing line printed above'
    end
  end

  # get all know systemd native units      from     systemctl list-units
  def read_all_known_units
    stdout_str, status = Open3.capture2(CMD_KNOWN_UNITS)
    return unless status.success?

    stdout_str.each_line do |l|
      match = RGXKU.match(l.strip)
      parse_know_unit(match)
    end
    pp :got_all_known_units
  end

  def parse_unit_from_file_list(match)
    if match
      f = match[1]
      return if f.end_with?('@') # ignore template units for now...

      if key?(f)
        self[f].state = match[2]
      else
        srv = SystemService.new(f)
        srv.state = match[2]
        srv.read_status_info
        self[srv] = srv if srv
      end
    else
      puts l
      raise 'Failed parsing line printed above'
    end
  end

  # add all know systemd  units  files  from  systemctl list-unit-files
  def add_units_from_file_list
    stdout_str, status = Open3.capture2(CMD_LIST_UNIT)
    return unless status.success?

    stdout_str.each_line do |l|
      match = RGXLI.match(l.strip)
      parse_unit_from_file_list(match)
    end
    #    pp :got_all_units_from_file
  end

  # get all legacy init.d scripts
  def add_init_d_scripts
    flist = Dir.glob("#{INIT}/*").reject { |f| INIT_IGNORE.match(f) }
               .map { |f| File.basename(f) }
    flist.each do |f|
      # only try to add an init script if it does not exist as an
      # native systemd unit, as these have precedence
      # these assumes we have to read native service before
      if key?(f)
        srv = self[f]
        srv.overrides_legacy = true
        srv.read_state
      else
        srv = SystemServiceLegacy.new(f)
        self[srv] = srv if srv
      end
    end
    #    pp :got_all_init_scripts
  end

  # get all maanged services as sepcified in config file
  def read_managed_services
    #    pp :getting_managed_services
    @config.managed_services.each do |f|
      # only try to add an init script if it does not exist already
      # in the catlog
      next if key?(f)

      srv = SystemService.new(f)
      srv.read_status_info
      srv.read_state
      self[srv] = srv if srv
    end
  end

  # get either
  #    all available units/scripts    OR
  #     the  anaged services only (as specified in the config file)
  def read_requested_services
    #    pp :getting_all_requested_services
    if @config.managed_services
      read_managed_services
    else
      read_all_known_units
      add_units_from_file_list
      add_init_d_scripts
      puts "Built service catalog with #{size} item(s)"
    end
    self
  end
end
