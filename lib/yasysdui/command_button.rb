require 'gtk3'
# Wrapper over Gtk::Button
class CommandButton < Gtk::Button
  attr_accessor :ctrl
  attr_accessor :actionstr
  def initialize(actionstr, ctrl)
    super(label: actionstr)
    @ctrl = ctrl
    @actionstr = actionstr
    signal_connect('clicked') { @ctrl.execute(@actionstr) }
  end
end
