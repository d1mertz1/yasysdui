require_relative 'index'
require 'open3'
require 'pp'

# base class for a systemd unit/service
class SystemServiceBase < String
  include Indexable
  attr_accessor :descr
  attr_accessor :state
  attr_accessor :load
  attr_accessor :active
  attr_accessor :sub
  attr_accessor :overrides_legacy
  attr_accessor :filename_full

  CMDINFO = 'sudo systemctl status'.freeze
  DESCRGX = /\A.+\.service\s+-\s+([[:print:]]+)\Z/
  DESCMASKRGX = /\A.+\.service\Z/
  LOADEDRGX = /\ALoaded:\s+([[:graph:]]+)\s+\((.*)\)\Z/
  ACTIVERGX = /\AActive:\s+([[:graph:]]+)\s+\(([[:graph:]]+)\).*\Z/
  WARNRGX = /\AWarning:/
  STAT_READ_FAIL_RGX = Regexp.new('\AFailed to get properties:.*\Z')
  STAT_NF_RGXRAW = '\AUnit\s[[:graph:]]+\scould\snot\sbe\sfound.\Z'.freeze
  STAT_NOT_FOUND_RGX = Regexp.new(STAT_NF_RGXRAW)
end

# for a systemd unit/service
class SystemService < SystemServiceBase
  def read_status_info_from_output(action_output)
    @status_info_raw = action_output
    parse_status_info
  end

  # This provides the texts to index (module Indexable)
  def each_sentence
    yield to_str
    yield @descr unless @descr.nil?
  end

  def parse_status_info
    return if handle_masked_info

    info_tab = @status_info_raw.lines
                               .reject { |l| WARNRGX.match(l) }
                               .map(&:strip)
    return nil if STAT_READ_FAIL_RGX.match(info_tab[0])

    return nil if STAT_NOT_FOUND_RGX.match(info_tab[0])

    # read description and
    # complete data
    return nil unless parse_descr_info(info_tab)

    parse_status_info_complete(info_tab)
  end

  def parse_descr_info(info_tab)
    if (match = DESCRGX.match(info_tab[0]))
      @descr = match[1]
    else
      # masked script/service without description returned
      unless (match = DESCMASKRGX.match(info_tab[0]))
        puts info_tab[0]
        raise 'Failed parsing line printed above'
      end
    end
    match
  end

  def parse_status_info_complete(info_tab)
    match = LOADEDRGX.match(info_tab[1])
    if match
      @load = match[1]
      load_info = match[2].split(';')
      @filename_full = load_info[0]
      @state = load_info[1] ? load_info[1].strip : nil
    end
    match = ACTIVERGX.match(info_tab[2].strip)
    return unless match

    @active = match[1]
    @sub = match[2]
  end

  def handle_masked_info
    if @state == 'masked'
      @load = 'masked'
      @active = 'inactive'
      @sub = 'dead'
      true
    else
      false
    end
  end

  def read_status_info
    return nil if end_with?('@')
    return if handle_masked_info

    @status_info_raw, @status = Open3.capture2("#{CMDINFO} #{self}")
    if !@status_info_raw.lines.empty?
      parse_status_info
    else
      # TODO: a more decent error handling...
      puts 'Failed getting service status info for'
      pp self
    end
  end

  def read_state_parse(stdout_str)
    info_tab = stdout_str.lines.reject { |l| WARNRGX.match(l) }
    return nil if STAT_READ_FAIL_RGX.match(info_tab[0].strip)

    match = LOADEDRGX.match(info_tab[1].strip)
    return unless match

    load_info = match[2].split(';')
    @filename_full = load_info[0]
    @state = load_info[1] ? load_info[1].strip : 'masked'
  end

  def read_state
    return nil if end_with?('@')

    stdout_str, @status = Open3.capture2("#{CMDINFO} #{self}")
    if !stdout_str.lines.empty?
      read_state_parse(stdout_str)
    else
      puts 'Failed getting service state info for'
      pp self
    end
  end

  # default constructor, takes unit name
  def initialize(str)
    super(str)
    @overrides_legacy = false
  end

  def native_info
    # yes+   : native and overrides init script
    # yes    : native and no init script
    # no     : init script
    if native?
      @overrides_legacy ? 'yes+' : 'yes'
    else
      'no'
    end
  end

  def native?
    true
  end
end

# Legacy service (ie not native, but with init script)
class SystemServiceLegacy < SystemService
  def initialize(str)
    super(str)
    read_status_info
  end

  def native?
    false
  end
end
