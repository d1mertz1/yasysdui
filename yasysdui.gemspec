D = 'A quick and dirty ruby-gtk3 ui wrapper around systemd/systemctl'.freeze
F = ['bin/yasysdui', 'lib/yasysdui.rb'] + Dir.glob('lib/yasysdui/*.rb')

Gem::Specification.new do |s|
  s.name        = 'yasysdui'
  s.version     = '1.0.4'
  s.summary     = 'Yet another systemd ui'
  s.description = D
  s.authors     = ['Denis Mertz']
  s.email       = 'dev@familie-mertz.eu'
  s.files       = F
  s.bindir      = 'bin'
  s.executables << 'yasysdui'
  s.add_runtime_dependency 'gtk3', '~> 3.2'
  s.add_runtime_dependency 'parseconfig', '~> 1.0', '>= 1.0.8'
  s.homepage = 'https://gitlab.com/d1mertz1/yasysdui'
  s.license = 'MIT'
end
