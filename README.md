# yasysdui - Yet another Systemd user interface
[![Language: Ruby](https://img.shields.io/badge/language-ruby-red.svg)](https://www.ruby-lang.org)
[![Gem Version](https://badge.fury.io/rb/yasysdui.svg)](https://badge.fury.io/rb/yasysdui)


This is a very simple brainless ui wrapper around _systemd_ _service_ _start/stop/status_.
It might eventually evolve in a real more usefull tool.

## Dependencies
* [ruby-gtk3](https://github.com/ruby-gnome2/ruby-gnome2/tree/master/gtk3)
* systemd
* sudo

## Installation 
The tool is a simple ruby script and uses the ruby-gtk3 library. It is available as a [rubygem](https://rubygems.org/gems/yasysdui "yasysdui gem").
You can install it with

	gem install yasysdui

## Usage
The tool executable is called 'yasysdui'. You can start it by executing in a terminal

	yasysdui

## Use cases
Lets assume you are forced to use some closed source third party tools that ship their own daemons/services 
(typically vpn clients), and for some reason you dont want to have these services running all the time
on your system, but on demand. Then this tool might be for you.
It permits you to start/stop on demand a selected set (hardcoded in the script) of services on demand.

## Personalisation
Per default all existing services units are loaded and displayed.
But you can restrict the list to a small set of services by maintaining a configuration file. This configuration should 
be located in the .config directory in your home directory and is named yasysdui.conf ; full path ~/.config/yasysdui.conf 
The syntax is very simple, it contains a variable "managed_services" with the list of service names separated by a space. Example content of this file is

	# this is a comment in the config file
	managed_services = vpnagent your_selfmade_service_foo exim4 

## Known issues
* #2 The startup is dead slow. This will be fixed in a future version by using dbus instead of system commands to collect the data .

## Caveats

* This tool is not part of the official systemd project
* I am not a systemd expert. *Use at your own risks.*

## How does it look like ?
### The full list on start 
![Full List](./images/yasysdui_full_list.png "Full list")

### Using the fastsearch for filtering 
![Filtered List with fastsearch](./images/yasysdui_fastsearch_udev.png "Filtered List with fastsearch")



