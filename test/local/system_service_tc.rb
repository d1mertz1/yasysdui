require 'test/unit'
require 'yasysdui/system_service'
require 'pry'
require 'pp'

class SystemServiceTestResult < SystemServiceBase
  attr_accessor :native_info
  def initialize(attribs)
    @descr = attribs[:descr]
    @native_info = attribs[:native_info]
    @state = attribs[:state]
    @load = attribs[:load]
    @active = attribs[:active]
    @sub = attribs[:sub]
    @overrides_legacy = attribs[:overrides_legacy]
    @filename_full = attribs[:filename_full]
  end
end

class TCSystemService < Test::Unit::TestCase
  def setup
    h1 = { active: 'active', native_info: 'yes', sub: 'running', \
           state: 'static' }
    h2 = { active: 'inactive', native_info: 'yes', sub: 'dead', \
           state: 'masked' }
    @expected = { 'systemd-logind' => SystemServiceTestResult.new(h1) }
    @expected['hostname'] = SystemServiceTestResult.new(h2)
  end

  def assert_attribs_equal(expected, other)
    #    assert_equal(expected.state, other.state)
    assert_equal(expected.native_info, other.native_info)
    assert_equal(expected.active, other.active)
    assert_equal(expected.sub, other.sub)
  end

  def test_read_status_info_1
    @serv = SystemService.new('systemd-logind')
    @serv.read_status_info
    assert_not_nil @serv
    assert_equal(true, @serv.native?)
    assert_attribs_equal(@expected['systemd-logind'], @serv)
  end

  def test_read_status_info_2
    @serv = SystemService.new('hostname')
    @serv.read_status_info
    assert_not_nil @serv
    assert_equal(true, @serv.native?)
    assert_attribs_equal(@expected['hostname'], @serv)
  end

  def test_read_state_1
    @serv = SystemService.new('systemd-logind')
    @serv.read_state
    assert_not_nil @serv
    assert_equal(@expected['systemd-logind'].state, @serv.state)
  end

  def test_read_state_2
    @serv = SystemService.new('hostname')
    @serv.read_state
    assert_not_nil @serv
    assert_equal(@expected['hostname'].state, @serv.state)
  end
end
