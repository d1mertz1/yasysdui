require 'test/unit'
require 'yasysdui/index'

class MockService < String
  include Indexable
  attr_accessor :descr
  def each_sentence
    yield to_str
    yield @descr
  end
end

class TCIndexable < Test::Unit::TestCase
  def setup
    @serv = MockService.new('avahi-daemon')
    @serv.descr = 'This is for'

    @serv2 = MockService.new('A.cD')
    @serv2.descr = 'cdE?'
  end

  def test_each_sentence
    ret = []
    @serv.each_sentence do |s|
      ret << s
    end
    assert ret.include?('avahi-daemon')
    assert ret.include?('This is for')
  end

  def test_each_word
    ret = []
    @serv.each_word { |w| ret << w }
    assert ret.include?('avahi')
    assert ret.include?('daemon')
    assert ret.include?('This')
    assert ret.include?('is')
    assert ret.include?('for')
    assert !ret.include?('Blurb')
  end

  def test_each_wordstart
    ret = []
    @serv2.each_wordstart { |ws| ret << ws }
    assert ret.include?('a')
    assert ret.include?('c')
    assert ret.include?('cd')
    assert ret.include?('cde')
    assert !ret.include?('?')
    assert !ret.include?('cde?')
  end
end
